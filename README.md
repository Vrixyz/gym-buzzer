The objective is to have a simple game to do sports with.

# How to play
- Activate Raspberry Pi
- Activate buttons
- Gameloop begins, a timer is displayed
  - A target button is lightened/written on a master screen
  - Tap on target button
- at the end of the timer, it's game over, compare your highest score !


# Evolutions
- RFID tag to put in weights, to move heavy objects as an exercise
- activate multiple buttons at once, with a timer for each buttons


# Technology

For now, [Rustlang](https://www.rust-lang.org) with [amethyst.rs](https://amethyst.rs) is chosen for the display, but I'm not sure it's working on Raspberry Pi.

A first version would have only physical buttons, but it's not practical to place buttons with wire, so a bluetooth communication between Raspberry Pi and buttons would make sense.

- The raspberry Pi is responsible for displaying information on a screen, game logic, and communication with buttons
- The buttons have as minimum logic as possible

## Sequence Diagram

```plantuml
actor Player as player
boundary "Button" as button
control "Raspberry Pi" as pi

player -> button : power up
player -> pi : start game
pi -> button : lighten()
player -> button : Tap
button -> pi : pushed()
pi -> button : darken()
pi -> pi : update score
pi -> pi : end game ?
```

## Resources
- To help with bluetooth communication : https://www.instructables.com/id/Arduino-Bluetooth-Master-and-Slave-Using-Any-HC-05/