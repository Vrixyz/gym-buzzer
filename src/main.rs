extern crate amethyst;

pub mod system;

use system::*;

use amethyst::{
    assets::Loader,
    core::TransformBundle,
    ecs::prelude::Entity,
    input::InputBundle,
    prelude::*,
    renderer::{DisplayConfig, DrawFlat, Pipeline, PosNormTex, RenderBundle, Stage},
    ui::{Anchor, TtfFormat, UiText, UiTransform},
    ui::{DrawUi, UiBundle},
    utils::application_root_dir,
};

struct Example;

impl SimpleState for Example {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;
        initialise_scoreboard(world);
        initialise_target_ui(world);
    }
}

/// Initialises a ui scoreboard
fn initialise_scoreboard(world: &mut World) {
    let font = world.read_resource::<Loader>().load(
        "font/square.ttf",
        TtfFormat,
        (),
        (),
        &world.read_resource(),
    );
    let score_transform = UiTransform::new(
        "P1".to_string(),
        Anchor::TopMiddle,
        0.,
        -50.,
        1.,
        200.,
        50.,
        0,
    );

    let score = world
        .create_entity()
        .with(score_transform)
        .with(UiText::new(
            font.clone(),
            "0".to_string(),
            [1., 1., 1., 1.],
            50.,
        ))
        .build();
    world.add_resource(score::ScoreText { score });
}

/// Initialises a ui scoreboard
fn initialise_target_ui(world: &mut World) {
    let font = world.read_resource::<Loader>().load(
        "font/square.ttf",
        TtfFormat,
        (),
        (),
        &world.read_resource(),
    );
    let target_transform = UiTransform::new(
        "Target".to_string(),
        Anchor::BottomMiddle,
        0.,
        50.,
        1.,
        400.,
        60.,
        0,
    );

    let target = world
        .create_entity()
        .with(target_transform)
        .with(UiText::new(
            font.clone(),
            "0".to_string(),
            [1., 1., 1., 1.],
            50.,
        ))
        .build();
    world.add_resource(target::TargetText { target });
}

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let path = format!("{}/resources/display_config.ron", application_root_dir());
    let binding_path = format!("{}/resources/bindings_config.ron", application_root_dir());
    let config = DisplayConfig::load(&path);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.00196, 0.23726, 0.21765, 1.0], 1.0)
            .with_pass(DrawFlat::<PosNormTex>::new())
            .with_pass(DrawUi::new()),
    );
    let input_bundle =
        InputBundle::<String, String>::new().with_bindings_from_file(binding_path)?;
    let game_data = GameDataBuilder::default()
        .with_bundle(RenderBundle::new(pipe, Some(config)).with_sprite_sheet_processor())?
        .with_bundle(TransformBundle::new())?
        .with_bundle(UiBundle::<String, String>::new())?
        .with_bundle(input_bundle)?
        .with(
            score::UpdateScoreSystem,
            "update_score_system",
            &["input_system"],
        )
        .with(
            target::DisplayTargetSystem,
            "display_target_system",
            &["update_score_system"],
        )
        .with(
            score::DisplayScoreSystem,
            "display_score_system",
            &["update_score_system"],
        );
    let mut game = Application::new("./", Example, game_data)?;
    game.run();
    Ok(())
}
