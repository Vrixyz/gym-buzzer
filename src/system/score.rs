use crate::*;

use amethyst::{
    ecs::prelude::{Read, ReadExpect, System, Write, WriteStorage},
    input::InputHandler,
    ui::UiText,
};

/// ScoreBoard contains the actual score data
#[derive(Default)]
pub struct ScoreBoard {
    pub score: i32,
}

pub struct UpdateScoreSystem;
impl<'s> System<'s> for UpdateScoreSystem {
    type SystemData = (
        Write<'s, ScoreBoard>,
        Write<'s, target::Target>,
        Read<'s, InputHandler<String, String>>,
    );

    fn run(&mut self, (mut scores, mut target, input): Self::SystemData) {
        if input.keys_that_are_down().count() > 1 {
            return;
        }
        if input.action_is_down(&target.action).unwrap_or(false) {
            scores.score += 1;
            target.set_new_action();
        }
    }
}

/// ScoreText contains the ui text components that display the score
pub struct ScoreText {
    pub score: Entity,
}
/// To display the score
pub struct DisplayScoreSystem;
impl<'s> System<'s> for DisplayScoreSystem {
    type SystemData = (
        WriteStorage<'s, UiText>,
        Read<'s, ScoreBoard>,
        ReadExpect<'s, ScoreText>,
    );

    fn run(&mut self, (mut ui_text, scores, score_text): Self::SystemData) {
        if let Some(text) = ui_text.get_mut(score_text.score) {
            text.text = scores.score.to_string();
        }
    }
}
