use crate::*;

use amethyst::{
    ecs::prelude::{Read, ReadExpect, System, WriteStorage},
    ui::UiText,
};

/// Helper for Target
const fn get_actions() -> [&'static str; 2] {
    ["buzzer_1", "buzzer_2"]
}
/// Target contains the next action the player should perform
pub struct Target {
    pub action: String,
    current_action_index: usize,
}
impl Default for Target {
    fn default() -> Self {
        Target {
            action: get_actions()[0].to_string(),
            current_action_index: 0,
        }
    }
}
impl Target {
    pub fn set_new_action(&mut self) {
        let all_actions = get_actions();
        self.current_action_index += 1;
        self.current_action_index %= all_actions.len();
        self.action = all_actions[self.current_action_index].to_string()
    }
}
/// TargetText contains the ui text components that display the next target
pub struct TargetText {
    pub target: Entity,
}
/// To display the target
pub struct DisplayTargetSystem;
impl<'s> System<'s> for DisplayTargetSystem {
    type SystemData = (
        WriteStorage<'s, UiText>,
        Read<'s, Target>,
        ReadExpect<'s, TargetText>,
    );

    fn run(&mut self, (mut ui_text, targets, target_text): Self::SystemData) {
        if let Some(text) = ui_text.get_mut(target_text.target) {
            text.text = targets.action.clone();
        }
    }
}
